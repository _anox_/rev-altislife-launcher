package eu.binbash.rev.launcher.gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.actions.CheckforUpdateAction;
import eu.binbash.rev.launcher.gui.components.RevPanel;


public class GUI implements ProgressUpdater {
	
	private JFrame frame;
	private JPanel mainPanel, optionPanel, cardPanel;
	
	//Options Panel
	private JButton backBut, forumBut, teamspeakBut, missionChooseBut, missionShowBut, logsShowBut, executableBut, saveBut, resetBut;
	private JTextField parametersField, missionPathField, executableField, remoteServerTestField, remoteServerRealField;
	
	//Main Panel
	private JButton optionsBut, updateBut, startBut, exitBut;
	private JCheckBox testServerCB;
	private JTextPane logArea;
	private JProgressBar progressBar;
	private JScrollPane logPaneScroller;
	
	private String bgPath = "/images/rev.jpg";
	
	public GUI() {
		buildGUI();
	}

	private void buildGUI() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(800, 450);
		frame.setResizable(false);

		createMainPanel();
		createOptionsPanel();
		createCardPanel();
		
		frame.add(this.cardPanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
	}
	
	private void createCardPanel() {
		cardPanel = new JPanel(new CardLayout());
		cardPanel.add(mainPanel, GuiType.Main.name());
		cardPanel.add(optionPanel, GuiType.Options.name());
	}

	private void createOptionsPanel() {
		optionPanel = new RevPanel(bgPath);
		optionPanel.setPreferredSize(new Dimension(800,450));
		optionPanel.setBackground(Color.DARK_GRAY);
		
		// Set/open Missionspath
		// Custom Executable
		// Custom Parameters
		// Arma3 Logs
		// Reset
		
		//private JButton backBut, forumBut, teamspeakBut, missionChooseBut, missionShowBut, logsShowBut, executableBut, saveBut, resetBut;
		//private JTextField parametersField, missionPathField, executableField;
		//private JLabel missionLab, executableLab, parametersLab;
		
		parametersField = new JTextField(50);
		parametersField.setToolTipText("[z.B. -maxVram=3584 -noSplash]");
		missionPathField = new JTextField(50);
		missionPathField.setToolTipText("[Missionspfadzur PBO Datei]");
		executableField= new JTextField(50);
		executableField.setToolTipText("[Alternative Arma3 Exe]");
		remoteServerTestField = new JTextField(50);
		remoteServerRealField = new JTextField(50);
		
		formatTextField(parametersField, "Arma3 Start-Parameter");
		formatTextField(missionPathField, "Pfad der Missionsdatei");
		formatTextField(executableField, "Pfad zur Arma3 Exe");
		formatTextField(remoteServerTestField, "Server-IP (Test-System)");
		formatTextField(remoteServerRealField, "Server-IP");
		
		forumBut = new JButton("Forum (direkt)");
		teamspeakBut = new JButton("Teamspeak (direkt)");
		missionShowBut = new JButton("Mission anzeigen (direkt)");
		logsShowBut = new JButton("Arma3 Logs (direkt)");
		
		missionChooseBut = new JButton("Missionpfad");
		executableBut = new JButton("Arma3 Exe");
		saveBut = new JButton("Save");
		resetBut = new JButton("Reset");
		backBut = new JButton("Zurück");

		formatButton(saveBut, "but_save.png", 640, 285, 145, 40);
		formatButton(forumBut, "but_forum.png", 200, 35, 145, 40);
		formatButton(teamspeakBut, "but_teamspeak.png", 345, 35, 145, 40);
		formatButton(missionChooseBut, "but_missionchoose.png", 40, 140, 145, 40);
		formatButton(executableBut, "but_exe.png", 40, 190, 145, 40);
		formatButton(missionShowBut, "but_missionopen.png", 490, 35, 145, 40);
		formatButton(logsShowBut, "but_logs.png", 635, 35, 145, 40);
		
		formatButton(resetBut, "but_reset.png", 200, 285, 145, 40);
		formatButton(backBut, "but_back.png", 640, 400, 145, 40);
		
		executableField.setSize(565, 40);
		parametersField.setSize(565, 40);
		missionPathField.setSize(565, 40);
		remoteServerTestField.setSize(565, 40);
		remoteServerRealField.setSize(565, 40);
		
		executableField.setLocation(200, 80);
		parametersField.setLocation(200, 120);
		missionPathField.setLocation(200, 160);
		remoteServerTestField.setLocation(200, 200);
		remoteServerRealField.setLocation(200, 240);
		
		optionPanel.add(executableField);
		optionPanel.add(parametersField);
		optionPanel.add(missionPathField);
		optionPanel.add(remoteServerTestField);
		optionPanel.add(remoteServerRealField);
		optionPanel.add(saveBut);
		optionPanel.add(forumBut);
		optionPanel.add(teamspeakBut);
		optionPanel.add(missionChooseBut);
		optionPanel.add(missionShowBut);
		optionPanel.add(logsShowBut);
		optionPanel.add(executableBut);
		optionPanel.add(resetBut);
		optionPanel.add(backBut);
	}

	private void formatTextField(JTextField textField, String title) {
		textField.setBorder(BorderFactory.createTitledBorder(title));
		textField.setBackground(new Color(255,156,0));
		textField.setForeground(Color.DARK_GRAY);
	}

	private void createMainPanel() {
		mainPanel = new RevPanel(bgPath);
		mainPanel.setPreferredSize(new Dimension(800,450));
		mainPanel.setBackground(Color.DARK_GRAY);
		
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setBackground(Color.DARK_GRAY);
		progressBar.setForeground(new Color(255,156,0));
		
		startBut = new JButton();
		optionsBut = new JButton();
		updateBut = new JButton();
		exitBut = new JButton();
		formatButton(startBut, "but_altis.png", 15, 275, 135, 40);
		formatButton(optionsBut, "but_optionen.png", 15, 325, 135, 40);
		formatButton(updateBut, "but_update.png", 295, 275, 135, 40);
		formatButton(exitBut, "but_verlassen.png", 295, 325, 135, 40);
		
		testServerCB = new JCheckBox("TestServer");
		testServerCB.setOpaque(false);
		createLogArea();
		
		logPaneScroller = new JScrollPane(logArea);
		
		logPaneScroller.setLocation(475, 55);
		logPaneScroller.setSize(300, 335);
		
		progressBar.setLocation(475, 15);
		progressBar.setSize(300, 35);
		
		testServerCB.setLocation(690, 420);
		testServerCB.setSize(105, 25);
		
		mainPanel.add(logPaneScroller);
		mainPanel.add(startBut);
		mainPanel.add(updateBut);
		mainPanel.add(optionsBut);
		mainPanel.add(exitBut);
		mainPanel.add(progressBar);
		mainPanel.add(testServerCB);
	}

	private void createLogArea() {
		logArea = new JTextPane();
		logArea.setEditable(false);
		logArea.setMaximumSize(logArea.getSize());
		logArea.setContentType("text/html");
		logArea.setBackground(Color.DARK_GRAY);
		logArea.setForeground(Color.ORANGE);
		final StyledDocument doc = logArea.getStyledDocument();
		addStylesToDocument(doc);
		doc.putProperty(DefaultEditorKit.EndOfLineStringProperty, "\n");
		logPaneScroller = new JScrollPane(logArea);
	}
	
	private void addStylesToDocument(final StyledDocument doc) {
		// Initialize some styles.
		final Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

		final Style regular = doc.addStyle("regular", def);
		StyleConstants.setFontSize(regular, 9);
		StyleConstants.setFontFamily(def, "SansSerif");

		final Style s = doc.addStyle("warnStyle", regular);
		StyleConstants.setForeground(s, Color.red);
	}

	private void formatButton(JButton button, String img, int x, int y, int width, int height) {
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		try {System.out.println("img["+img+"]");
		    Image butImg = ImageIO.read(getClass().getResource("/images/"+img));
		    button.setIcon(new ImageIcon(butImg));
		  } catch (IOException ex) {}
		button.setLocation(x, y);
		button.setSize(width, height);
	}
	
	public void trace(final String trace) {
		trace(trace, false);
	}
	
	public void trace(final String trace, final boolean warn) {
		final StyledDocument doc = logArea.getStyledDocument();
		try {
			doc.insertString(doc.getLength(), String.format("%s\n", trace), doc.getStyle(warn ? "warnStyle" : "regular"));
			
			int x;
			logArea.selectAll();
			x = logArea.getSelectionEnd();
			logArea.select(x,x);
		} catch (final BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	public void showGUI(boolean show) {
		frame.setVisible(show);
	}
	
	public void setOptionsAction(ActionListener al) {
		optionsBut.addActionListener(al);
	}
	
	public void setBackAction(ActionListener al) {
		backBut.addActionListener(al);
	}
	
	public void setUpdateAction(ActionListener al) {
		updateBut.addActionListener(al);
	}
	
	public void setStartGameAction(ActionListener al) {
		startBut.addActionListener(al);
	}
	
	public void setExitAction(ActionListener al) {
		exitBut.addActionListener(al);
	}
	
	public void setSaveAction(ActionListener al) {
		saveBut.addActionListener(al);
	}
	
	public void setForumAction(ActionListener al) {
		forumBut.addActionListener(al);
	}
	
	public void setTeamspeakAction(ActionListener al) {
		teamspeakBut.addActionListener(al);
	}
	
	public void setResetAction(ActionListener al) {
		resetBut.addActionListener(al);
	}
	
	public void setMissionPathAction(ActionListener al) {
		missionChooseBut.addActionListener(al);
	}
	
	public void setMissionShowAction(ActionListener al) {
		missionShowBut.addActionListener(al);
	}
	
	public void setShowArmaLogsAction(ActionListener al) {
		logsShowBut.addActionListener(al);
	}
	
	public void setChooseArmaExeAction(ActionListener al) {
		executableBut.addActionListener(al);
	}

	public void loadParameters(Parameters p) {
		executableField.setText(p.getExecutable());
		missionPathField.setText(p.getMissionPath());
		parametersField.setText(p.getParameters());
		remoteServerTestField.setText(p.getRemoteServerTest());
		remoteServerRealField.setText(p.getRemoteServerReal());
	}

	public void setServerSwitchAction(CheckforUpdateAction checkforUpdateAction) {
		this.testServerCB.addActionListener(checkforUpdateAction);
	}

	@Override
	public void setProgressMinMax(int min, int max) {
		progressBar.setMaximum(max);
		progressBar.setMinimum(min);
		progressBar.setValue(min);
	}

	@Override
	public void setProgressIndeterminate(boolean indeterminate) {
		progressBar.setIndeterminate(indeterminate);
	}

	@Override
	public void setProgressString(String txt) {
		progressBar.setString(txt);
	}

	@Override
	public void setProgressValue(int val) {
		progressBar.setValue(val);
	}

	@Override
	public void setProgress(String txt, int value) {
		setProgress(txt, value, Color.DARK_GRAY);
	}
	
	@Override
	public void setProgress(String txt, int value, Color c) {
		progressBar.setString(txt);
		progressBar.setValue(value);
		if(c != null) {progressBar.setBackground(c);}
	}
	
	public boolean isTestServer() {
		return testServerCB.isSelected();
	}

	public void enableUpdateButton(boolean enable) {
		updateBut.setEnabled(enable);
	}

	public void switchCard(GuiType guiType) {
		((CardLayout) cardPanel.getLayout()).show(cardPanel,guiType.name());
	}

	public Parameters getParameters() {
		Parameters p = new Parameters();
		p.setExecutable(executableField.getText().trim());
		p.setMissionPath(missionPathField.getText().trim());
		p.setParameters(parametersField.getText().trim());
		p.setRemoteServerTest(remoteServerTestField.getText().trim());
		p.setRemoteServerReal(remoteServerRealField.getText().trim());
		return p;
	}

	public void reloadParameters() {
		loadParameters(OptionsHandler.getInst().getParameters());
	}

}
