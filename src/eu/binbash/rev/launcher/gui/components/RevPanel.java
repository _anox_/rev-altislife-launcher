package eu.binbash.rev.launcher.gui.components;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class RevPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private BufferedImage img;

	public RevPanel(final String imgPath) {
		try {
			img = ImageIO.read(getClass().getResource(imgPath));
		}catch(Exception e) {
			e.printStackTrace();
		}
		setLayout(null);
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
    }	
}
