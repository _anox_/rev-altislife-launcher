package eu.binbash.rev.launcher.gui;

import java.awt.Color;

public interface ProgressUpdater {

	public void setProgressValue(int val);
	public void setProgressMinMax(int min, int max);
	public void setProgressIndeterminate(boolean indeterminate);
	public void setProgressString(String txt);
	public void setProgress(String txt, int value);
	public void setProgress(String txt, int value, Color c);
	
}
