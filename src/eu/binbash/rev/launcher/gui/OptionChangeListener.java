package eu.binbash.rev.launcher.gui;

import eu.binbash.rev.launcher.Parameters;

public interface OptionChangeListener {

	public void optionsHaveChanged(Parameters p);
	
}
