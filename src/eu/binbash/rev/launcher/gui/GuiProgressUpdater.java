package eu.binbash.rev.launcher.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.TimerTask;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * Keeps track of File Download Progression
 *  
 * @author SchreiberS
 */
public class GuiProgressUpdater {
	
	private Timer timer;
	
	public GuiProgressUpdater() {
	}

	public void startDownloadTracker(final GUI gui, final int targetSize, final File targetFile) {
		if(targetFile == null || targetSize <= 0) {
			gui.trace("Fehler im Download Tracker.", true);
		}
		
		ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
				//Update Progressbar
				if(targetFile.exists()) {
					long targetFileSize = targetFile.length();
					int mbLoaded = (int)(targetFileSize / 1024 / 1024);
					gui.setProgress("Lade Datei "+mbLoaded+" / "+targetSize+"mb", mbLoaded);
				}
            }
        };
		
		
		gui.setProgressIndeterminate(false);
		gui.setProgressMinMax(0, targetSize);
		
		timer = new Timer(0, taskPerformer);
		timer.start();
	}
	
	public void stopDownloadTracker() {
		if(this.timer != null) {
			this.timer.stop();
		}
	}
}
