package eu.binbash.rev.launcher;

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import eu.binbash.rev.launcher.gui.GUI;
import eu.binbash.rev.launcher.gui.GuiProgressUpdater;
import eu.binbash.rev.launcher.util.FileFinder;
import eu.binbash.rev.launcher.util.FileFinder.DirType;
import eu.binbash.rev.launcher.util.HttpHandler;
import eu.binbash.rev.launcher.util.MD5CheckSum;

public class PBOHandler {

	public static String remoteTestUrl = "http://rev.binbash.eu/pbo/test/";
	public static String remoteRealUrl = "http://rev.binbash.eu/pbo/real/";

	public static String pboFileReal = "rev_Life.Altis";
	public static String pboFileTest = "rev_test.Altis";
	
	public static String pboSuffix = "pbo";

	private static String remoteUrl;
	private static String pboFile;

	private GuiProgressUpdater guiUpdater;
	private GUI gui;

	public PBOHandler(GUI gui) {
		this.gui = gui;
		this.guiUpdater = new GuiProgressUpdater();
	}

	public boolean checkForUpdate(File steamFolder) {
		boolean updateNeeded = false;
		setServerEnvironment();

		gui.setProgressIndeterminate(true);
		steamFolder = autofindSteamFolder(steamFolder);

		if(steamFolder != null && steamFolder.exists()) {
			gui.trace("PBO Gefunden: "+pboFile);
			// Get local Pbo File
			File localPboFile = FileFinder.findFile(pboFile+"."+pboSuffix, steamFolder);

			String localPboMD5 = null;
			
			if(localPboFile != null && localPboFile.exists()) {
				localPboMD5 = MD5CheckSum.getMD5Checksum(localPboFile);
				gui.trace("own md5:"+localPboMD5);
	
				// Get remote MD5
				String remoteMD5 = readRemoteMD5(gui);
				gui.trace("remote md5:"+remoteMD5);
	
				// Check against own
				if(remoteMD5.compareToIgnoreCase(localPboMD5) != 0) {
					gui.trace("Update verfügbar!");
					boolean fileIsNotLocked = localPboFile.renameTo(localPboFile); // hack oder legit? file.canWrite() ist buggy.
					if(fileIsNotLocked) {
						gui.setProgress("Update verfügbar!",0,Color.RED);
						updateNeeded = true;
					}else {
						gui.trace("Missionsdatei kann nicht geschrieben werden. Bitte Arma beenden.", true);
					}
				}else {
					gui.trace("Missionsdatei scheint aktuell zu sein.");
					gui.setProgress("Missionsdatei ist aktuell!",0,Color.GREEN);
				}
			}else {
				gui.trace("Konnte keine lokale PBO finden. Brauche Update.", true);
				gui.setProgress("Update verfügbar!",0,Color.RED);
				updateNeeded = true;
			}

		}else {
			gui.setProgressString("Missions Ordner nicht gefunden.");
			gui.trace("Missions Ordner nicht gefunden. Bitte Konfigurieren!", true);
		}

		return updateNeeded;
	}

	private void setServerEnvironment() {
		remoteUrl = gui.isTestServer() ? remoteTestUrl : remoteRealUrl;
		pboFile = gui.isTestServer() ? pboFileTest : pboFileReal;
	}

	public File autofindSteamFolder(File steamFolder) {
		if(steamFolder == null) {
			gui.setProgressIndeterminate(true);
			gui.setProgressString("Suche Arma3 PBO....");
			gui.trace("Suche Arma3 Missionsdatei...");
			steamFolder = FileFinder.findMissionFolder(DirType.Mission);
			gui.reloadParameters();
			gui.setProgressIndeterminate(false);
		}
		return steamFolder;
	}

	public void handleUpdate(File steamFolder) {

		try {
			setServerEnvironment();
			steamFolder = autofindSteamFolder(steamFolder);

			if(checkForUpdate(steamFolder)) {

				int pboSize = HttpHandler.getContentSize(remoteUrl+pboFile+"."+pboSuffix) / 1024 / 1024;
				File targetFile = File.createTempFile(pboFile, pboSuffix);

				guiUpdater.startDownloadTracker(gui, pboSize, targetFile);

				File remotePboFile = HttpHandler.fetchFileFromUrl(remoteUrl+pboFile+"."+pboSuffix, targetFile);
//				String remotepboMd5 = MD5CheckSum.getMD5Checksum(remotePboFile);
//				gui.trace("pbo:"+remotepboMd5+" precalculated:"+remoteMD5);
				//Compare remotePboFile with fetched checksum
//				if(remoteMD5.compareToIgnoreCase(remotepboMd5) == 0) {
				// If correct copy
//				gui.trace("Downloaded PBO scheint in Ordnung zu sein.");
				File localPboFile = FileFinder.findFile(pboFile+"."+pboSuffix, steamFolder);
				if(localPboFile == null || !localPboFile.exists()) {
					localPboFile = new File(steamFolder.getAbsolutePath() + "/" + pboFile+"."+pboSuffix);
				}
				Files.copy(remotePboFile.toPath(), localPboFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				gui.trace("Fertig.");
//				}else {
//				gui.trace("Downloaded PBO korrupt oder md5 missmatch.",true);
//				}
			}

		}catch(Exception e) {
			e.printStackTrace();
			gui.trace("Fehler:"+e.getMessage(), true);
		}finally {
			gui.setProgressIndeterminate(false);
			guiUpdater.stopDownloadTracker();
		}
	}

	private String readRemoteMD5(GUI gui) {
		String remoteMD5 = null;
		char[] charBuffer = new char[100];

		try {
			File targetFile = File.createTempFile("lifepbo_md5", "txt");
			File remoteMD5File = HttpHandler.fetchFileFromUrl(remoteUrl + "lifepbo_md5.txt", targetFile);
			if(remoteMD5File != null && remoteMD5File.canRead()) {
				FileReader fr = new FileReader(remoteMD5File);
				fr.read(charBuffer);
				fr.close();
				remoteMD5 = String.copyValueOf(charBuffer).trim();
				remoteMD5 = remoteMD5.substring(0, 32);
			}
		}catch(Exception e) {
			e.printStackTrace();
			gui.trace("Fehler readRemoteMD5:"+e.getMessage(), true);
		}

		return remoteMD5;
	}

}
