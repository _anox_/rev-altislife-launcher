package eu.binbash.rev.launcher.actions;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JOptionPane;

import eu.binbash.rev.launcher.util.FileFinder;
import eu.binbash.rev.launcher.util.FileFinder.DirType;

public class MissionShowAction implements ActionListener {

	private DirType type;

	public MissionShowAction(DirType type) {
		this.type = type;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
			File missionDir = FileFinder.findMissionFolder(type);
		
		try {
			if(missionDir != null) {
				Desktop.getDesktop().open(missionDir);	
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Konnte ["+missionDir.getAbsolutePath()+"] nicht öffnen!\n"+e.getMessage());
			e.printStackTrace();
		}
	}

}
