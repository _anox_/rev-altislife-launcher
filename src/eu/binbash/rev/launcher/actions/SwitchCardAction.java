package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import eu.binbash.rev.launcher.gui.GUI;
import eu.binbash.rev.launcher.gui.GuiType;

public class SwitchCardAction implements ActionListener {

	private GUI gui;
	private GuiType guiType;

	public SwitchCardAction(GUI gui, GuiType guiType) {
		this.gui = gui;
		this.guiType = guiType;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		gui.switchCard(guiType);
	}

}
