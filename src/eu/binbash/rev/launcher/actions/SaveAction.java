package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.gui.GUI;

public class SaveAction implements ActionListener {

	private GUI gui;

	public SaveAction(GUI gui) {
		this.gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Parameters p = gui.getParameters();
		OptionsHandler.getInst().saveParameters(p);
		JOptionPane.showMessageDialog(null, "Erfolgreich abgespeichert!");
	}

}
