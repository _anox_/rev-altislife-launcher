package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.gui.GUI;
import eu.binbash.rev.launcher.util.HttpHandler;

public class StartGameAction implements ActionListener {

	private GUI gui;

	public StartGameAction(GUI gui) {
		this.gui = gui;
	}
	
	/*
	 * 
	 * "B:\Program Files\Steam\SteamApps\common\Arma 3\arma3battleye.exe" 0 1 -enableHT -maxMem=8192 -maxVram=3584 -malloc=tbbmalloc -noBenchmark  -noPause -noSplash
	 * 0%201%20-enableHT%20-maxMem=8192%20-maxVram=3584%20-malloc=tbbmalloc%20-noBenchmark%20-noPause%20-noSplash
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			Parameters p = OptionsHandler.getInst().getParameters();
			
			String executionStr = p.getExecutable();
			String connectStr = gui.isTestServer() ? p.getRemoteServerTest() : p.getRemoteServerReal();
			connectStr = " -connect="+connectStr+" -port=2302";
			
			if(executionStr.contains("exe")) {
				executionStr += " " + p.getParameters() + connectStr;
				executeRuntime(executionStr);
			}else {
				String moddedParams = modifyParameters(p.getParameters() + connectStr);
				executionStr += moddedParams;
				executeDesktop(executionStr);
			}
			
			//String ip = gui.isTestServer() ? PBOHandler.remoteServerTest : PBOHandler.remoteServerReal;
			//gui.trace("Starte Arma3 -> "+ip);
			//Desktop.getDesktop().browse(new URI("steam://run/107410//0%201%20-enableHT%20-maxMem=8192%20-maxVram=3584%20-malloc=tbbmalloc%20-noBenchmark%20-noPause%20-noSplash%20-connect="+ip+"%20-port=2302"));
			//Desktop.getDesktop().browse(new URI("steam://run/107410//-connect="+ip+"%20-port=2302"));
		} catch (Exception e1) {
			e1.printStackTrace();
			gui.trace("Konnte Arma3 nicht starten. ["+e1.getMessage()+"]");
		}
	}

	private String modifyParameters(String string) {
		return string.replaceAll(" ", "%20");
	}

	private void executeDesktop(String executionStr) {
		try {
			HttpHandler.openWebpage(new URI(executionStr));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private void executeRuntime(String executionStr) {
		try {
			Runtime.getRuntime().exec(executionStr);
			//Process process = new ProcessBuilder(executionStr).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
