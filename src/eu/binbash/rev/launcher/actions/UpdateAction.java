package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import eu.binbash.rev.launcher.Launcher;
import eu.binbash.rev.launcher.gui.GUI;

public class UpdateAction implements ActionListener {

	private GUI gui;
	private Launcher launcher;

	public UpdateAction(Launcher launcher, GUI gui) {
		this.launcher = launcher;
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.gui.enableUpdateButton(false);
		try {
			launcher.performAltisLifeUpdate();
		}catch(Exception e1) {
			this.gui.trace("Fehler beim Updaten: "+e1.getMessage());
		}finally {
			this.gui.enableUpdateButton(true);
		}
	}

}
