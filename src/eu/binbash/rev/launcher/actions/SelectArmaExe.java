package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.gui.GUI;

public class SelectArmaExe implements ActionListener {

	private JFileChooser chooser;
	private GUI gui;
	private String choosertitle = "Arma3 Exe angeben.";

	public SelectArmaExe(GUI gui) {
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "Bitte gewünschte Arma3 exe auswählen.\nZum Beispiel: arma3.exe oder arma3battleye.exe");
		
		File exeFile = getExeFile();
		
		if(exeFile != null && !exeFile.isDirectory()) {
			// Save ExeFile
			Parameters p = OptionsHandler.getInst().getParameters();
			p.setExecutable(exeFile.getAbsolutePath());
			OptionsHandler.getInst().saveParameters(p);
			gui.reloadParameters();
		}
	}

	private File getExeFile() {
		chooser = new JFileChooser(); 
	    chooser.setDialogTitle(choosertitle);
	    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

	    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
	      System.out.println("getSelectedFile() : " 
	         +  chooser.getSelectedFile());
	      return chooser.getSelectedFile();
	      }
	    else {
	      System.out.println("No Selection ");
	      }
		return null;
	}

}
