package eu.binbash.rev.launcher.actions;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;

import javax.swing.JOptionPane;

import eu.binbash.rev.launcher.util.HttpHandler;

public class OpenUrlAction implements ActionListener {

	private String url;
	private File file;

	public OpenUrlAction(String url) {
		this.url=url;
	}
	
	public OpenUrlAction(File file) {
		this.file = file;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		try {
			if(file != null) {
				Desktop.getDesktop().open(file);	
			}else if(url != null) {
				HttpHandler.openWebpage(new URI(url));
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Konnte ["+url+"] nicht öffnen!\n"+e.getMessage());
			e.printStackTrace();
		}
	}

}
