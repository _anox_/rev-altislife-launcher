package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import eu.binbash.rev.launcher.Launcher;
import eu.binbash.rev.launcher.gui.GUI;

public class CheckforUpdateAction implements ActionListener {

	private GUI gui;
	private Launcher launcher;

	public CheckforUpdateAction(Launcher launcher, GUI gui) {
		this.launcher = launcher;
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			launcher.checkForUpdate();
		}catch(Exception e1) {
			this.gui.trace("Fehler beim Checken: "+e1.getMessage());
		}
	}

}
