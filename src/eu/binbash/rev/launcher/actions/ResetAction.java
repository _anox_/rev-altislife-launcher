package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.gui.GUI;

public class ResetAction implements ActionListener {

	private GUI gui;

	public ResetAction(GUI gui) {
		this.gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Parameters p = OptionsHandler.getInst().restoreStandardParameters();
		gui.loadParameters(p);
	}

}
