package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExitAction implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		System.exit(0); 
	}

}
