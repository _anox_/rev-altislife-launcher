package eu.binbash.rev.launcher.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import eu.binbash.rev.launcher.Launcher;
import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;
import eu.binbash.rev.launcher.gui.GUI;

public class MissionFileAction implements ActionListener {

	private Launcher control;
	
	private JFileChooser chooser;
	private GUI gui;
	private String choosertitle = "Steam Order angeben.";

	public MissionFileAction(Launcher control, GUI gui) {
		this.control = control;
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "Bitte Arma3 MPMissionsCache Ordner auswählen.\n Liegt normalerweise in %Appdata%/Local/Arma3");
		
		File steamFolder = getSteamFolder();
		
		if(steamFolder != null && steamFolder.isDirectory()) {
			control.setSteamFolder(steamFolder);
			// Save MissionFile Directory
			Parameters p = OptionsHandler.getInst().getParameters();
			p.setMissionPath(steamFolder.getAbsolutePath());
			OptionsHandler.getInst().saveParameters(p);
			gui.reloadParameters();
			//control.performAltisLifeUpdate();
		}
	}

	private File getSteamFolder() {
		String startingDir = System.getenv("LOCALAPPDATA") != null ? System.getenv("LOCALAPPDATA")+"/Arma 3/MPMissionsCache" : ".";
		
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new File(startingDir));
	    chooser.setDialogTitle(choosertitle);
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

	    // disable the "All files" option.
	    chooser.setAcceptAllFileFilterUsed(false);
	    
	    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
	      System.out.println("getCurrentDirectory(): " 
	         +  chooser.getCurrentDirectory());
	      System.out.println("getSelectedFile() : " 
	         +  chooser.getSelectedFile());
	      return chooser.getCurrentDirectory();
	      }
	    else {
	      System.out.println("No Selection ");
	      }
		return null;
	}

}
