package eu.binbash.rev.launcher.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import eu.binbash.rev.launcher.Parameters;

public class Serializer {

	private static final String SER_FILE = "revLauncher.ini";
	private static final String SER_DIRECTORY_PATH = System.getProperty("user.home");

	public static void serializeObj(final Object o) {
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(probeDir(), SER_FILE));
			oos = new ObjectOutputStream(fos);
			oos.writeObject(o);
		} catch (IOException e) {
			System.out.println("[Serializer]  Fehler beim Serialisieren:");
			e.printStackTrace();
		} finally {
			if (oos != null)
				try {
					oos.close();
				} catch (IOException e) {
					System.out.println("[Serializer]  Fehler beim Serialisieren:");
					e.printStackTrace();
				}
			if (fos != null)
				try {
					fos.close();
				} catch (IOException e) {
					System.out.println("[Serializer]  Fehler beim Serialisieren:");
					e.printStackTrace();
				}
		}
	}

	private static File probeDir() {
		final File serDirectory = new File(SER_DIRECTORY_PATH);
		return serDirectory;
	}

	public static Parameters loadSavedParameters() {
		Parameters result = null;

		try{
			FileInputStream fin = new FileInputStream(new File(probeDir(), SER_FILE));
			ObjectInputStream ois = new ObjectInputStream(fin);
			result = (Parameters) ois.readObject();
			ois.close();
		} catch (Exception e){
			System.out.println("[Serializer]  Konnte das User.home Directory nicht finden..."+e.getMessage());
		}

		return result;
	}

}
