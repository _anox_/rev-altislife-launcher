package eu.binbash.rev.launcher.util;

import java.io.File;

import eu.binbash.rev.launcher.OptionsHandler;
import eu.binbash.rev.launcher.Parameters;

public class FileFinder {

	public enum DirType {
		Mission, Logs
	}

	public static File findMissionFolder(DirType dirType) {
		if (dirType == DirType.Logs) {
			if (System.getenv("LOCALAPPDATA") != null) {
				return new File(System.getenv("LOCALAPPDATA") + "/Arma 3");
			}
		} else {
			File steamDir = null;
			String missionPath = OptionsHandler.getInst().getParameters().getMissionPath();
			if (missionPath != null && !missionPath.isEmpty()) {
				steamDir = new File(missionPath);
			} else if (System.getenv("LOCALAPPDATA") != null) {
				steamDir = new File(System.getenv("LOCALAPPDATA") + "/Arma 3/MPMissionsCache");
			}

			if (steamDir != null && steamDir.isDirectory()) {
				// Save MissionFile Directory
				Parameters p = OptionsHandler.getInst().getParameters();
				p.setMissionPath(steamDir.getAbsolutePath());
				OptionsHandler.getInst().saveParameters(p);

				return steamDir;
			}
		}
		return null;
	}

	public static File findFile(final String filename, File startDir) {
		File f = null;
		for (File temp : startDir.listFiles()) {
			if (temp.isDirectory()) {
				f = findFile(filename, temp);
				if (f != null)
					return f;
			} else {

				if (filename.compareToIgnoreCase(temp.getName()) == 0) {
					return temp;
				}
			}
		}
		return null;
	}
}
