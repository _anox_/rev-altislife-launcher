package eu.binbash.rev.launcher.util;

import java.awt.Desktop;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class HttpHandler {

	public static File fetchFileFromUrl(String url, File target) {
//		File target = null;
		try {
//			target = File.createTempFile(tmpFilePrefix, tmpFileSuffix);
			URL website = new URL(url);
			Files.copy(website.openStream(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return target;
	}
	
	public static int getContentSize(String urlString) {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urlString);
		    conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("HEAD");
		    conn.getInputStream();
		    return conn.getContentLength();
		 } catch (Exception e) {
		        return -1;
		 } finally {
		        conn.disconnect();
		 }
		
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}

	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	}
	
}
