package eu.binbash.rev.launcher;

import eu.binbash.rev.launcher.util.Serializer;

public class OptionsHandler  {

	private String missionPath;
	private String executable;
	private String parameters;
	private String remoteServerTest;
	private String remoteServerReal;

	private String std_missionPath;
	private String std_executable = "steam://run/107410//";
	private String std_parameters = "";
	private String std_remoteServerTest = "176.28.9.73";
	private String std_remoteServerReal = "37.59.1.50";

	private static OptionsHandler inst;

	public static OptionsHandler getInst() {
		if(inst == null) {
			inst = new OptionsHandler();
		}
		return inst;
	}

	private OptionsHandler() {

		//Pre-init
		std_missionPath = "";
		missionPath = std_missionPath;
		executable = std_executable;
		parameters = std_parameters;
		remoteServerTest = std_remoteServerTest;
		remoteServerReal = std_remoteServerReal;

		if(checkSaveDir()) {
			loadSaved();
		}
	}

	private boolean checkSaveDir() {
		return true;//TODO
	}

	public Parameters loadSaved() {
		Parameters p = Serializer.loadSavedParameters();
		if(p != null) {
			if(p.getExecutable() != null && !p.getExecutable().isEmpty()) {
				executable = p.getExecutable();
			}
			if(p.getMissionPath() != null && !p.getMissionPath().isEmpty()) {
				missionPath = p.getMissionPath();
			}
			if(p.getParameters() != null && !p.getParameters().isEmpty()) {
				parameters = p.getParameters();
			}
			if(p.getRemoteServerTest() != null && !p.getRemoteServerTest().isEmpty()) {
				remoteServerTest = p.getRemoteServerTest();
			}
			if(p.getRemoteServerReal() != null && !p.getRemoteServerReal().isEmpty()) {
				remoteServerReal = p.getRemoteServerReal();
			}
		}
		return p;
	}

	public void saveParameters(Parameters p) {
		missionPath = p.getMissionPath();
		parameters = p.getParameters();
		executable = p.getExecutable();
		remoteServerReal = p.getRemoteServerReal();
		remoteServerTest = p.getRemoteServerTest();

		Serializer.serializeObj(p);
	}

	public void saveParameters() {
		Parameters p = new Parameters();
		p.setExecutable(executable);
		p.setMissionPath(missionPath);
		p.setParameters(parameters);
		p.setRemoteServerReal(remoteServerReal);
		p.setRemoteServerTest(remoteServerTest);
		Serializer.serializeObj(p);
	}

	public Parameters getParameters() {
		Parameters p = new Parameters();
		p.setExecutable(executable);
		p.setMissionPath(missionPath);
		p.setParameters(parameters);
		p.setRemoteServerReal(remoteServerReal);
		p.setRemoteServerTest(remoteServerTest);
		return p;
	}

	public Parameters restoreStandardParameters() {
		missionPath = std_missionPath;
		executable = std_executable;
		parameters = std_parameters;
		remoteServerReal = std_remoteServerReal;
		remoteServerTest = std_remoteServerTest;

		Parameters p = new Parameters();
		p.setExecutable(executable);
		p.setMissionPath(missionPath);
		p.setParameters(parameters);
		p.setRemoteServerReal(remoteServerReal);
		p.setRemoteServerTest(remoteServerTest);

		return p;
	}


}
