package eu.binbash.rev.launcher;

import java.io.File;

import eu.binbash.rev.launcher.actions.CheckforUpdateAction;
import eu.binbash.rev.launcher.actions.ExitAction;
import eu.binbash.rev.launcher.actions.MissionFileAction;
import eu.binbash.rev.launcher.actions.MissionShowAction;
import eu.binbash.rev.launcher.actions.OpenUrlAction;
import eu.binbash.rev.launcher.actions.ResetAction;
import eu.binbash.rev.launcher.actions.SaveAction;
import eu.binbash.rev.launcher.actions.SelectArmaExe;
import eu.binbash.rev.launcher.actions.StartGameAction;
import eu.binbash.rev.launcher.actions.SwitchCardAction;
import eu.binbash.rev.launcher.actions.UpdateAction;
import eu.binbash.rev.launcher.gui.GUI;
import eu.binbash.rev.launcher.gui.GuiType;
import eu.binbash.rev.launcher.util.FileFinder.DirType;

public class Launcher {

	private GUI gui;
	private File steamFolder;
	private boolean isUpdating = false;
	private PBOHandler pboHandler;

	public Launcher() {
		loadParameters();
		initGUI();
		pboHandler = new PBOHandler(gui);

		// Autoperform check on startup
		checkForUpdate();
	}

	private void loadParameters() {
		OptionsHandler.getInst().loadSaved();
	}

	public boolean checkForUpdate() {
		return pboHandler.checkForUpdate(steamFolder);
	}

	public boolean isUpdatePending() {
		if(isUpdating) {
			gui.trace("Update noch im Gange! Bitte warten.");
			return true;
		}
		return false;
	}

	public void performAltisLifeUpdate() {
		if(!isUpdatePending()) {
			isUpdating = true;
			new Thread(new Runnable() {

				@Override
				public void run() {
					pboHandler.handleUpdate(steamFolder);
					isUpdating = false;

				}
			}).start();
		}
	}

	private void initGUI() {
		gui = new GUI();

		gui.setExitAction(new ExitAction());
		gui.setStartGameAction(new StartGameAction(gui));
		gui.setOptionsAction(new SwitchCardAction(gui, GuiType.Options));
		gui.setBackAction(new SwitchCardAction(gui, GuiType.Main));
		gui.setUpdateAction(new UpdateAction(this, gui));
		gui.setServerSwitchAction(new CheckforUpdateAction(this, gui));
		gui.setSaveAction(new SaveAction(gui));
		gui.setForumAction(new OpenUrlAction("http://www.diefreimaurer.com"));
		gui.setTeamspeakAction(new OpenUrlAction("ts3server://ts.diefreimaurer.com"));
		gui.setResetAction(new ResetAction(gui));
		gui.setMissionPathAction(new MissionFileAction(this, gui));
		gui.setMissionShowAction(new MissionShowAction(DirType.Mission));
		gui.setShowArmaLogsAction(new MissionShowAction(DirType.Logs));
		gui.setChooseArmaExeAction(new SelectArmaExe(gui));

		gui.loadParameters(OptionsHandler.getInst().getParameters());
		gui.showGUI(true);
	}

	public void setSteamFolder(File steamFolder) {
		this.steamFolder = steamFolder;
	}

}
