package eu.binbash.rev.launcher;

import java.io.Serializable;

public class Parameters implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String missionPath;
	private String executable;
	private String parameters;
	private String remoteServerTest;
	private String remoteServerReal;
	
	public Parameters() {}
	
	public String getMissionPath() {
		return this.missionPath;
	}
	
	public void setMissionPath(String missionPath) {
		this.missionPath = missionPath;
	}
	
	public String getExecutable() {
		return this.executable;
	}
	
	public void setExecutable(String executable) {
		this.executable = executable;
	}
	
	public String getParameters() {
		return this.parameters;
	}
	
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	
	public String getRemoteServerTest() {
		return this.remoteServerTest;
	}
	
	public void setRemoteServerTest(String remoteServerTest) {
		this.remoteServerTest = remoteServerTest;
	}
	
	public String getRemoteServerReal() {
		return this.remoteServerReal;
	}
	
	public void setRemoteServerReal(String remoteServerReal) {
		this.remoteServerReal = remoteServerReal;
	}

}
